# K3S deployement with Cilium

# Description
This project aims to deploy k3s single node with cilium to manage all network functionnality which are:
- kube-proxy
- cni

## Dependencies
| Dependency | Version |
| ---------- | ------- |
| k3s | v1.28.3 |
| cilium | v1.15.0-pre.2 |
| ingress-nginx |  |
| cert-manager |  |

